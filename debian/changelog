brp-pacu (2.1.2+git20210421-1) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat

  [ Sebastian Ramacher ]
  * debian/watch: Switch to github
  * New upstream version 2.1.2+git20210421
  * debian/control:
    - Update BDs for new git snapshot
    - Switch to GTK+ 3 (Closes: #967275)
    - Bump Standards-Version
    - Update Homepage
    - Bump to debhelper-compat 13
    - Bump Standards-Version
  * debian/patches: Removed, no longer needed

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 05 Jan 2022 15:00:02 +0100

brp-pacu (2.1.1+git20111020-7) unstable; urgency=medium

  * Drop dbg package in favour of dbgsym.
  * Avoid useless linking.
  * Sign tags.
  * Update copyright file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 29 Dec 2016 14:09:33 +0100

brp-pacu (2.1.1+git20111020-6) unstable; urgency=medium

  * Set dh/compat 10.
  * Bump Standards.
  * Fix VCS fields.
  * Remove menu file.
  * Fix hardening.
  * Remove obsolete libgtkdatabox-0.9.2-0-dev from B-D. (Closes: #845537)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 24 Nov 2016 13:25:47 +0100

brp-pacu (2.1.1+git20111020-5) unstable; urgency=medium

  * Bump Standards.
  * Update copyright file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 10 Jun 2015 10:44:35 +0200

brp-pacu (2.1.1+git20111020-4) unstable; urgency=low

  * Fix the FTBFS with clang:
    - Fixed return void bug in gui.c
      Thanks to Arthur Marble <arthur@info9.net>
  * Bump Standards.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 20 Feb 2014 01:03:53 +0100

brp-pacu (2.1.1+git20111020-3) unstable; urgency=low

  * Removed DMUA.
  * Fix VCS canonical URLs.
  * Don't sign tags.
  * Added local-option file.
  * Fix build-depends on libgtkdatabox-dev. (Closes: #712878)
  * Added Keywords entry to desktop file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 06 Aug 2013 00:12:42 +0200

brp-pacu (2.1.1+git20111020-2) unstable; urgency=low

  * Set build dependency libgtkdatabox-dev to really close bug.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 03 Jun 2013 17:59:14 +0200

brp-pacu (2.1.1+git20111020-1) unstable; urgency=low

  * Set build dependency libgtkdatabox-0.9-2-0-dev. (Closes: #710913)
  * Bump Standards
  * Set dh/compat 9 to fix hardening.
  * Update copyright file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 03 Jun 2013 17:08:03 +0200

brp-pacu (2.1.1+git20110314~repack1-2) unstable; urgency=low

  * Team upload.
  * debian/control: Synopsis should not start with an article.
  * Update debian/get-git-source.sh to actually reflect the repacking.
  * Including individual glib headers no longer supported.
    Thanks to Michael Biebl for the patch. (Closes: #665513)
  * Update debian/copyright.
  * Bump Standards-Version.

 -- Alessio Treglia <alessio@debian.org>  Fri, 30 Mar 2012 09:13:17 +0200

brp-pacu (2.1.1+git20110314~repack1-1) unstable; urgency=low

  * Initial Release. (closes: #537881)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 17 Feb 2011 03:37:36 +0100
